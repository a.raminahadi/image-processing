<link rel="stylesheet" href="Style.css" type="text/css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.7/semantic.min.css">

<?php

    include_once("Resize_Convert.php");

    // Recieved file properties
    $fileName = $_FILES["uploaded_file"]["name"];
    $fileTmpLoc = $_FILES["uploaded_file"]["tmp_name"];
    $fileType = $_FILES["uploaded_file"]["type"];
    $fileSize = $_FILES["uploaded_file"]["size"];
    $fileErrorMsg = $_FILES["uploaded_file"]["error"];
    $fileName = preg_replace('#[^a-z.0-9]#i', '', $fileName);
    $Seperate = explode(".", $fileName);
    $fileExt = end($Seperate);

        // Error Handling 
        if (!$fileTmpLoc) {
            echo "ERROR: Please browse for a file before clicking the upload button.";
            exit();
        } else if($fileSize > 5242880) {
            echo "ERROR: Your file was larger than 5 Megabytes in size.";
            unlink($fileTmpLoc);
            exit();
        } else if (!preg_match("/.(gif|jpg|png)$/i", $fileName) ) {
            echo "ERROR: Your image was not .gif, .jpg, or .png.";
            unlink($fileTmpLoc);
            exit();
        } else if ($fileErrorMsg == 1) {
            echo "ERROR: An error occured while processing the file. Try again.";
            exit();
        }
        // End of error handling

    $moveResult = move_uploaded_file($fileTmpLoc, "uploads/$fileName");
        if ($moveResult != true) {
            echo "ERROR: File not uploaded. Try again.";
            exit();
        }

    // Resizing Function 
    $target_file = "uploads/$fileName";
    $resized_file = "uploads/resized_$fileName";
    Resize($target_file, $resized_file, $fileExt);
    unlink($target_file);
    // End of resizing

    // Convert Function
    if (strtolower($fileExt) != "jpg") {
        $target_file = "uploads/resized_$fileName";
        $new_jpg = "uploads/resized_".$Seperate[0].".jpg";
        Convert($target_file, $new_jpg, $fileExt);
        unlink($target_file);
    }
    // End of converting
    $file = "uploads/resized_".$Seperate[0].".jpg";
    $im = imagecreatefromjpeg($file);
        
        if($im && imagefilter($im, IMG_FILTER_GRAYSCALE)){
                imagejpeg($im, $file);
        }
        else
            {
        echo 'Conversion to grayscale failed.';
            }
        imagedestroy($im);

    $dir = "uploads";       
    if ($opendir = opendir($dir)){
        echo "<div id='container'>";
        echo "<div id='header'>";
            echo "<div><a href='index.php'><i class='chevron circle left icon'></i></a></div>";
        echo "</div>";
        echo "<div class='ui five column grid'>";
        while(($file = readdir($opendir)) !== FALSE){
            if($file!="." && $file!=".."){ 
                echo "<div class='column'>";
                echo "<div id='img-div' class='ui segment'>";
                echo "<img class='cicular medium ui image' src='$dir/$file'><br>";
                echo "<a href='download.php?file=$dir/$file'>Download Image</a>";
                echo "</div>";
                echo "</div>";
                
            }         
        }
        echo "</div>";
        echo "</div>";
        echo "<div id='footer' style='margin-top:2%; text-align:center'>Copyright 2017</div>";
    }
