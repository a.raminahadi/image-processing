<html>
    <head>
        <meta charset="utf-8">
        <title>Image Processing App</title>
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.7/semantic.min.css">
        <style>
            body {
                font-family: Arial, Helvetica, sans-serif;
            }
            #uploadPhoto-div {
                border:1px solid #ccc;
                color:#ccc;
                text-align:center;
                padding: 1% 0;
                margin:2% auto;
                width:12%;
            }
            #uploadPhoto-div:hover {
                cursor: pointer;
                background:#000;
                color: white;
            }
        
            .header {
                text-align: center;
                margin: 2%;
            }
            #img {
                display: none;
                margin: 0 auto;
                text-align: center;
            }
        </style>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
        <script>
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#img').show();
                        $('#img-div').show();
                        $('#uploadPhoto-div').hide();
                        $('.blah').attr('src', e.target.result);
                    };
                reader.readAsDataURL(input.files[0]);
                }
            }
        
        </script>
    </head>
<body>
    <div>
        <div class="header"><h2>Welcome to App</h2></div>
        <div id="uploadPhoto-div" onclick="$('#filePhoto').click()">Upload Photo
        </div>
            <form id="myform" action="upload.php" method="post" enctype="multipart/form-data">
                <div style="display:none">
                    <input type="file" name="uploaded_file" id="filePhoto" accept="image/*" onchange="readURL(this);">
                </div>
                <div id="img-div">
                    <div id="img">
                        <img src="#" class="blah">
                        <div style="display:none">
                            <button id="submit-btn">submit</button>
                        </div>
                        <div id="uploadPhoto-div" onclick="$('#submit-btn').click()">Submit Photo</div>
                    </div>
                </div>
            </form>
    </div>
</body>
</html>