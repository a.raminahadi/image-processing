# Image processing with web technologies 
This is a small (micro) service that allows users to do the following:
- upload an arbitrary image from the local filesystem 
- scale and crop to 200x200px 
- convert to greyscale 
- compress to jpg 
- display the final image along with a permanent download link. 

## Instructions

- Download and locate the project inside the directory root of your web server
- Browse it


